
var request = require('request');
var jsdom = require('jsdom');
var fs = require("fs");
var jquery = fs.readFileSync("./jquery-1.6.min.js", "utf-8");
var util = require('util');

var stores = [
  {url : "https://itunes.apple.com", selector : "#title div h1"},
  {url : "https://play.google.com", selector : "div .document-title"},
  {url : 'http://www.windowsphone.com', selector : '#application h1[itemprop="name"]'},
]

var products = {}

var GC=0;

function getProdNameByUrl(url, selector, callback){
	GC++;

request({
  url: url,
  headers: {
  // User-Agent needed to prevent server protection
  'User-Agent':'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36'
 }
}, function (error, response, body) {

    //Check for error
    if(error){
    	GC--;
        return console.log('request:  Error:', error);
    }

    //Check for right status code
    if(response.statusCode !== 200){
    	GC--;
        return console.log('request: Invalid Status Code Returned:', response.statusCode, url);
    }

    //console.log("url",url);
    //console.log("selector",selector);

    jsdom.env({
    html: body,
    src: [jquery],
    done: function (err, window) {

    if(err){
    	GC--;
        return console.log('jsdom:  Error:', err);
    }

     var $ = window.jQuery;

    // console.log($('body').html());
    var name = $(selector).text();
    if(callback) callback(name.trim(),url);
    else GC--;
    }
   });

});
}


if (process.argv.length<3){
	console.log('usage: node productcrawler.js urls.txt');
	process.exit();
}

//console.log(process.argv.length, process.argv[2])
var urlFile = process.argv[2];
fs.readFile(urlFile, "utf8",function (err, data) {
  if (err){
	console.log('error reading file',urlFile, err);
	process.exit();
  }
  var urls=data.split('\n');
  urls.forEach(function(u){
  	//console.log(u);
  	for(var i=0;i<stores.length;i++){
  	 var s=stores[i];
     if(u.search(s.url)>=0){
      //console.log(s.url, s.selector);
      getProdNameByUrl(u, s.selector, function(prodName,url){
        var originalProdName = prodName;
        prodName=prodName.split(/for|-|:/i)[0].trim();
        GC--;
        if(prodName==undefined || prodName.length==0){
        	return
        }

//        console.log(GC,"prodName",prodName);
        if(!(prodName in products)){
        	products[prodName]={urls:[{url:url,name:originalProdName}]};
        }
        else{
        	products[prodName].urls.push({url:url,name:originalProdName});
        }

        if(GC==0){
        	//console.log('end');
        	//console.log(util.inspect(products, false, null));
        	for(product in products) {
        		console.log(product);
            }
        }
      });       
      break;
     }
  	};
  });
     
});

